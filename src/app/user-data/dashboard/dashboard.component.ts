import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { AuthService } from 'src/app/services/auth.service';
import { ProfileUser } from 'src/app/models/user';
import { ViewprofileComponent } from '../viewprofile/viewprofile.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild(ViewprofileComponent)
  public view!: ViewprofileComponent;
  user$ = this.usersService.currentUserProfile$;

  constructor(private usersService: UsersService ) {}


  ngOnInit(): void {
  }

}
