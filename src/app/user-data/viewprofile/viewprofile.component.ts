import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HotToastService } from '@ngneat/hot-toast';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { switchMap, tap } from 'rxjs';

import { ProfileUser } from 'src/app/models/user';
import { ImageUploadService } from 'src/app/services/image-upload.service';
import { UsersService } from 'src/app/services/users.service';

@UntilDestroy()
@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.scss']
})
export class ViewprofileComponent implements OnInit {
  user$ = this.usersService.currentUserProfile$;
  firstlength=0;
  lastlength=0;
  ablength=0;
  x=62.5;

  profileForm = new FormGroup({
    uid: new FormControl(''),
    displayName: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    phone: new FormControl(''),
    address: new FormControl(''),
    city: new FormControl(''),
    country: new FormControl(''),
    aboutme: new FormControl('')
  });

  constructor(
    private imageUploadService: ImageUploadService,
    private toast: HotToastService,
    private usersService: UsersService,
    private route:Router
  ) {}

  ngOnInit(): void {
    this.usersService.currentUserProfile$
      .pipe(untilDestroyed(this), tap(console.log))
      .subscribe((user) => {
        this.profileForm.patchValue({ ...user });
      });

      this.profileForm.get('firstName')?.valueChanges.subscribe((value:String)=>{
        this.firstlength=value.length;
        if((this.firstlength>0 &&this.lastlength==0 &&this.ablength==0) ||(this.firstlength==0 &&this.lastlength!==0 &&this.ablength==0)
        ||(this.firstlength==0 &&this.lastlength==0 &&this.ablength!=0)){
          this.x=75;
        }
      })
      this.profileForm.get('lastName')?.valueChanges.subscribe((value:String)=>{
        this.lastlength=value.length;
        if((this.firstlength!=0 &&this.lastlength!=0 &&this.ablength==0) ||(this.firstlength==0 &&this.lastlength!==0 &&this.ablength!=0)
        ||(this.firstlength!=0 &&this.lastlength==0 &&this.ablength!=0)){
          this.x=87.5;
        }
      })
      this.profileForm.get('aboutme')?.valueChanges.subscribe((value:String)=>{
        this.ablength=value.length;
        if(this.ablength!=0 &&this.firstlength!=0 &&this.lastlength!=0){
          this.x=100;
        }
      })


  //  this.profileForm.get('firstName')?.statusChanges.subscribe(status=>{
  //   this.firstlength=status;
  //       if((this.firstlength=='VALID' && this.lastlength=='INVALID' &&this.ablength=='INVALID')
  //       ||(this.firstlength=='INVALID' &&this.lastlength=='VALID' &&this.ablength=='INVALID')
  //         ||(this.firstlength=='INVALID' &&this.lastlength=='INVALID' &&this.ablength=='VALID')){
  //         this.x=75;
  //       }

  //     })
  //     this.profileForm.get('lastName')?.statusChanges.subscribe(status1=>{
  //       this.lastlength=status1;
  //       if((this.firstlength=='VALID' && this.lastlength=='VALID' &&this.ablength=='INVALID')
  //       ||(this.firstlength=='INVALID' &&this.lastlength=='VALID' &&this.ablength=='VALID')
  //         ||(this.firstlength=='VALID' &&this.lastlength=='INVALID' &&this.ablength=='VALID')){
  //         this.x=87.5;
  //       }
  //     })
  //     this.profileForm.get('aboutme')?.statusChanges.subscribe(status2=>{
  //       this.ablength=status2;
  //       if(this.ablength=='VALID' &&this.firstlength=='VALID' &&this.lastlength=='VALID'){
  //         this.x=100;
  //       }
  //     })

  }

  uploadFile(event: any, { uid }: ProfileUser) {
    this.imageUploadService
      .uploadImage(event.target.files[0], `images/profile/${uid}`)
      .pipe(
        this.toast.observe({
          loading: 'Uploading profile image...',
          success: 'Image uploaded successfully',
          error: 'There was an error in uploading the image',
        }),
        switchMap((photoURL) =>
          this.usersService.updateUser({
            uid,
            photoURL,

          })
        )
      )
      .subscribe();
  }

  saveProfile() {
    const profileData = this.profileForm.value;
    this.usersService
      .updateUser(profileData)
      .pipe(
        this.toast.observe({
          loading: 'Saving profile data...',
          success: 'Profile updated successfully',
          error: 'There was an error in updating the profile',
        })

      )
      .subscribe(() => {
        this.route.navigate(['/dashboard']);

});
}

}

