import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import {
  canActivate,
  redirectLoggedInTo,
  redirectUnauthorizedTo,
} from '@angular/fire/auth-guard';

import { CrudComponent } from './post/crud/crud.component';
import { DialogComponent } from './post/dialog/dialog.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { TaskComponent } from './task/task.component';
import { DashboardComponent } from './user-data/dashboard/dashboard.component';
import { ViewprofileComponent } from './user-data/viewprofile/viewprofile.component';
import { LandingComponent } from './user-data/landing/landing.component';
import { LoginComponent } from './user-management/login/login.component';
import { SignupComponent } from './user-management/signup/signup.component';
import { PagenotfoundComponent } from './user-data/pagenotfound/pagenotfound.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLoggedInToDashboard = () => redirectLoggedInTo(['dashboard']);

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: LandingComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
    ...canActivate(redirectLoggedInToDashboard),
  },
  {
    path: 'signup',
    component: SignupComponent,
    ...canActivate(redirectLoggedInToDashboard),
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'viewprofile',
    component: ViewprofileComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'post',
    component: CrudComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'crud',
    component: CrudComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  {
    path: 'task',
    component: TaskComponent,
    ...canActivate(redirectUnauthorizedToLogin),
  },
  { path: '**', pathMatch: 'full', component: PagenotfoundComponent },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}},

  ]
})
export class AppRoutingModule {}
