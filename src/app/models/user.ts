export interface ProfileUser {
  uid: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  displayName?: string;
  phone?: number;
  address?: string;
  photoURL?: string;
  city?:string;
  country?:string;
  aboutme?:string;
}
