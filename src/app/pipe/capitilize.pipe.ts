import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitilize'
})
export class CapitilizePipe implements PipeTransform {

  transform(e:any): string {
    let first = e.substr(0,1).toUpperCase();
    return first + e.substr(1);
  }
}
