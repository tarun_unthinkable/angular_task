// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'angular-signup-d987c',
    appId: '1:157362340302:web:33614f706235291fcf2b9f',
    storageBucket: 'angular-signup-d987c.appspot.com',
    apiKey: 'AIzaSyDHVU1Y2QQsAI1I6u2f5pgjz0plaFp6r8g',
    authDomain: 'angular-signup-d987c.firebaseapp.com',
    messagingSenderId: '157362340302',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
